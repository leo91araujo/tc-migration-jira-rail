import requests
import re
import json
from model import *
from config import *

def parsePreConditions(preCondition):
    parsed = re.findall('(?<=\|)(.*?)(?=\|)', preCondition)
    return "1) Environment Details: \n {} \n Credentials: {}".format(parsed[13], parsed[17])

def parseTestSteps(steps):    
    stepsList = list()

    parsed = re.findall('(?<=\|)(.*?)(?=\|)', steps)
    
    for i in range(4, len(parsed) - 1, 4):
        stepsList.append(TestStep(parsed[i+1], parsed[i+2].replace('<br>',"\n>")))    

    return stepsList

def parseTestCase(response):  
    if response.status_code != 200:
        print "Not able to find Test Case on Jira"
        return None

    jiraResponse = response.json()
    key = str(jiraResponse['key'])
    summary = str(jiraResponse['fields']['summary'])
    jiraDescritpion = str(jiraResponse['fields']['description']).replace('\n', '<br>').replace('*', '')
    businessGoal = re.search('Business Goal(.*)Pre-conditions', jiraDescritpion).group(1).replace('<br>', '')
    preCondition = parsePreConditions(re.search('Pre-conditions(.*)Scenario', jiraDescritpion).group(1))
    steps = parseTestSteps(re.search('Scenario(.*)', jiraDescritpion).group(1))    

    return TestCase(key, summary, businessGoal, key, preCondition, steps)

def getJiraTestCase(key):
    jiraQueryUrl = JIRA_BASE_URL + "/issue/{}/"
    response = requests.get(jiraQueryUrl.format(key), headers={"Authorization":"Basic <BASE64_AUTH>"}, verify=False)
    return parseTestCase(response)