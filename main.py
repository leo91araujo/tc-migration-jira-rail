import requests
import re
import json
import model
import jira as jira
import testrail as rail

def main():
    requests.packages.urllib3.disable_warnings()

    rallyId = raw_input("Jira Test Case to migrate: ")     

    print "Reading Test Case Information from Jira"                    

    jiraTestCase = jira.getJiraTestCase(rallyId)                
    
    if jiraTestCase != None:
        rail.createTestCase(jiraTestCase)    

if __name__ == "__main__":
    main()

