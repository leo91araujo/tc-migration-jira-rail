import requests
import json
from model import *
from config import *

def sendRequest(http_method, url, payload):
    url = TEST_RAIL_BASE_URL + url            
    headers = {'Content-Type': "application/json", 'Authorization': "Basic <BASE64_AUTH>"}
    return requests.request(http_method, url, data=payload, headers=headers, verify=False)        

def createSection(testCase):
    
    #TODO: Add method to validate if section exists

    sectioName = "{}: {}".format(testCase.key, testCase.summary)  
    payload = "{\"name\":\"" + sectioName + "\"}"    
    response = sendRequest("POST", "/add_section/1", payload)
    return response.json()["id"] 

def createTestCase(testCase):         
    sectionId = createSection(testCase)
    testCase.section_id = sectionId

    payload = testCase.toJSON()
    url = "/add_case/" + str(testCase.section_id)
    response = sendRequest("POST", url, payload)
    
    print "Test Case [id = {}] created at Test Rail project".format(response.json()["id"])    