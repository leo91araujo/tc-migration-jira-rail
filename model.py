import json

class TestCase:
    key = ""
    summary = ""
    title = ""
    refs = ""
    custom_preconds = ""    
    custom_steps_separated = list()
    section_id = ""
    priority_id = ""
    template_id = ""
    type_id = ""

    def __init__(self, key, summary, title, refs, preConditions, steps):
        self.key = key
        self.summary = summary
        self.title = title
        self.refs = refs
        self.custom_preconds = preConditions
        self.custom_steps_separated = steps  
        self.section_id = 0
        self.priority_id = 2
        self.template_id = 2
        self.type_id = 7  

    def toJSON(self):
        return json.dumps(self, default=lambda obj: obj.__dict__)#, sort_keys=True, indent=4)
     

class TestStep:    
    content = ""
    expected = ""    

    def __init__(self, content, expected):        
        self.content = content
        self.expected = expected